import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User } from './users.schema';
import { Model } from 'mongoose';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<User>,
  ) {}

  async findUserByEmail(email: string) {
    const user = await this.userModel.findOne({ email }).lean().exec();
    return user;
  }

  create(email: string, password: string) {
    return this.userModel.create({ email, password });
  }
}
