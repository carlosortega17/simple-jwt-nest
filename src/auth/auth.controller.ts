import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { SingInDto } from './dto/singInDto';
import { AuthGuard } from './auth.guard';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @HttpCode(HttpStatus.OK)
  @Post('singIn')
  singIn(@Body() singInDto: SingInDto) {
    return this.authService.singIn(singInDto.email, singInDto.password);
  }

  @HttpCode(HttpStatus.CREATED)
  @Post('singUp')
  singUp(@Body() singInDto: SingInDto) {
    return this.authService.singUp(singInDto.email, singInDto.password);
  }

  @UseGuards(AuthGuard)
  @Get('profile')
  getProfile(@Request() req) {
    return req.user;
  }
}
