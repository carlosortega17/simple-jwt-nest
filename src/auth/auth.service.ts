import {
  ConflictException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';

import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async singIn(email: string, password: string) {
    const user = await this.userService.findUserByEmail(email);
    if (!user) {
      throw new NotFoundException('User not found');
    }
    if (!bcrypt.compareSync(password, user.password)) {
      throw new UnauthorizedException('Password error');
    }
    const { password: pwd, ...data } = user;
    const jwt = await this.jwtService.signAsync(data);
    return {
      jwt,
    };
  }

  async singUp(email: string, password: string) {
    const user = await this.userService.findUserByEmail(email);
    if (user) {
      throw new ConflictException('User allready exists');
    }
    await this.userService.create(email, bcrypt.hashSync(password, 10));
    return {
      message: 'created',
    };
  }
}
